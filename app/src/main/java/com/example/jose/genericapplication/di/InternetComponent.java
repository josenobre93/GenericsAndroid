package com.example.jose.genericapplication.di;

import com.example.jose.genericapplication.http.modules.ImdbApiModule;
import com.example.jose.genericapplication.http.modules.MovieDBApiModule;
import com.example.jose.genericapplication.topmovies.TopMoviesActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, ImdbApiModule.class, MovieDBApiModule.class})
public interface InternetComponent {
    public void inject(TopMoviesActivity topMoviesActivity); // digo que vou injectar o presenter no top movie activities(que contem todos estes modulos)
}

//TODO criar um application component que albergue apenas o application module em vez de estar a por tudo no mesmo componente(posi só é possivel injectar um de cada vez)