package com.example.jose.genericapplication.http.services;

import com.example.jose.genericapplication.http.model.ImdbApi;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;


/*

    Para obter mais informação dos filmes mais populares(informação que vem de outra API)

 */
public interface ImdbApiService {

    @GET("/")
    Observable<ImdbApi> getCountry(@Query("t") String title);
}
