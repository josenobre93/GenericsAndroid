package com.example.jose.genericapplication.di;

import android.app.Application;
import android.content.Context;

import dagger.Module;

@Module
public class ApplicationModule {

    private Application application;

    public ApplicationModule(Application application){
        this.application = application;
    }

    public Context provideContext(){
        return application;
    }

}
