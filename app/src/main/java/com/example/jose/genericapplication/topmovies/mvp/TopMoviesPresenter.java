package com.example.jose.genericapplication.topmovies.mvp;

import com.example.jose.genericapplication.topmovies.adapter.model.Movie;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class TopMoviesPresenter implements TopMoviesMVP.Presenter {

    private TopMoviesMVP.View view;
    private Disposable subscription = null;
    private TopMoviesMVP.Model model;


    public TopMoviesPresenter(TopMoviesMVP.Model model) {

        this.model = model;
    }

    @Override
    public void loadData() {

        subscription = model
                .result()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Movie>() {
                    @Override
                    public void onNext(Movie movie) {

                        if(view!= null){
                            view.updateData(movie);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                        e.printStackTrace();
                        if(view!= null){
                            view.showSnackBar("Error getting movies");
                        }

                    }

                    @Override
                    public void onComplete() {

                        //data é mostrada ao utilizador

                    }
                });

    }

    @Override
    public void rxUnsubscribe() {
        if(subscription!= null){
            if(!subscription.isDisposed()){
                subscription.dispose();
            }
        }

    }

    @Override
    public void setView(TopMoviesMVP.View view) {
        this.view = view;
    }
}
