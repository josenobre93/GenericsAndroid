package com.example.jose.genericapplication.topmovies.di;

import com.example.jose.genericapplication.http.modules.MovieDBApiModule;
import com.example.jose.genericapplication.http.services.ImdbApiService;
import com.example.jose.genericapplication.http.services.MovieDbApiService;
import com.example.jose.genericapplication.topmovies.mvp.TopMoviesMVP;
import com.example.jose.genericapplication.topmovies.mvp.TopMoviesModel;
import com.example.jose.genericapplication.topmovies.mvp.TopMoviesPresenter;
import com.example.jose.genericapplication.topmovies.repository.Repository;
import com.example.jose.genericapplication.topmovies.repository.RepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module (includes = {MovieDBApiModule.class})
public class TopMoviesModule {

    @Provides
    public TopMoviesMVP.Presenter provideTopMoviesActivityPresenter(TopMoviesMVP.Model topMoviesModel){
        return new TopMoviesPresenter(topMoviesModel);
    }

    @Provides
    public TopMoviesMVP.Model provideTopMoviesActivityModel(Repository repository){
        return new TopMoviesModel(repository);
    }

    @Provides
    @Singleton // so vai criar uma instância do nosso repository implemantation class
    public Repository provideRepository(MovieDbApiService movieDbApiService, ImdbApiService imdbApiService){
        return new RepositoryImpl(movieDbApiService,imdbApiService);
    }
}
