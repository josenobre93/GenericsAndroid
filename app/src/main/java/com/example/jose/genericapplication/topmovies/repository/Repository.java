package com.example.jose.genericapplication.topmovies.repository;

import com.example.jose.genericapplication.http.model.ImdbApi;
import com.example.jose.genericapplication.http.model.Result;

import io.reactivex.Observable;


/*

    Comunica com a API directamente

 */
public interface Repository {



    Observable<Result> getTopMoviesFromNetwork();
    Observable<String> getCountriesFromNetwork();


}
