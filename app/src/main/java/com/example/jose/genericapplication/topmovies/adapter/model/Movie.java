package com.example.jose.genericapplication.topmovies.adapter.model;

public class Movie {

    private String country;
    private String name;

    public Movie(String title, String s) {

    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
