package com.example.jose.genericapplication.topmovies.mvp;

import com.example.jose.genericapplication.http.model.Result;
import com.example.jose.genericapplication.topmovies.repository.Repository;
import com.example.jose.genericapplication.topmovies.adapter.model.Movie;

import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;

public class TopMoviesModel implements TopMoviesMVP.Model{

    private Repository repository;

    public TopMoviesModel(Repository repository){
        this.repository = repository;
    }
    //TODO perceber o que é que é este filha da puta do Result(ver o filha da puta dos videos)
    //TODO ver video do fiho da puta a explicar esta parte caralho
    @Override
    public Observable<Movie> result() {

        // O observable .zip aceita dois observadores como argumentos e através de uma lógica de função
        // FUNÇÃO do zip : Combiná-los num só observador ( zipar tudo)

        return Observable.zip(
                repository.getTopMoviesFromNetwork(), //obtenho os filmes todos
                repository.getCountriesFromNetwork(), // obtenho o pais para cada um dos paises que recebo na de cima

                //passo o model do adapter,String e o que quero ver na view
                new BiFunction<Result, String, Movie>() {
                    @Override
                    public Movie apply(Result result, String s) throws Exception {
                        return new Movie(result.title,s);
                    }
                }
        );
    }
}
