package com.example.jose.genericapplication.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.CLASS) // manter a anotação durante a classe
public @interface ApplicationScope {
}


//TODO NOT used
