package com.example.jose.genericapplication.topmovies.mvp;

import com.example.jose.genericapplication.topmovies.adapter.model.Movie;

import io.reactivex.Observable;

public interface TopMoviesMVP {

    interface View{
        void updateData(Movie movie);
        void showSnackBar(String s);
    }

    interface Presenter{
        void loadData();
        void rxUnsubscribe();
        void setView(TopMoviesMVP.View view);
    }

    interface Model{
        Observable<Movie> result();
    }
}
