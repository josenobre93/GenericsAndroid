package com.example.jose.genericapplication.topmovies.repository;

import com.example.jose.genericapplication.http.model.ImdbApi;
import com.example.jose.genericapplication.http.model.MovieDbApi;
import com.example.jose.genericapplication.http.model.Result;
import com.example.jose.genericapplication.http.services.ImdbApiService;
import com.example.jose.genericapplication.http.services.MovieDbApiService;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;


//TODO ver onde é que o filha da puta pos as cenas do mockito
// TODO A SABER Esta é a class que usa o java reactive, ou seja é aqui que faço as chamadas ás APIS e uso o Java RX para a actividade em especifico.
public class RepositoryImpl implements Repository {

    // 2 apis
    private MovieDbApiService movieDbApiService;
    private ImdbApiService imdbApiService;

    // declarar listas
    private List<String> countries;
    private List<Result> results;
    private long timestamp;


    public RepositoryImpl(MovieDbApiService movieDbApiService, ImdbApiService imdbApiService) {
        this.movieDbApiService = movieDbApiService;
        this.imdbApiService = imdbApiService;
        countries = new ArrayList<>();
        results = new ArrayList<>();
        this.timestamp  = System.currentTimeMillis();
    }



    //obter os filmes mais populares da internet
    @Override
    public Observable<Result> getTopMoviesFromNetwork() {

        // estou a obter 3 resultados de 3 páginas diferentes
        Observable<MovieDbApi> topRatedObservable = movieDbApiService.getTopRatedMovies(1)
                .concatWith(movieDbApiService.getTopRatedMovies(2))
                .concatWith(movieDbApiService.getTopRatedMovies(3));

        // nao uso flatmap porque não preserva a ordem dos elementos dessa maneira uso o concat map porque quero preserar a ordem dos objectos retornados de 3 apis calls
        return topRatedObservable.concatMap(new Function<MovieDbApi, ObservableSource<? extends Result>>() {
            @Override
            public ObservableSource<? extends Result> apply(MovieDbApi movieDbApi) throws Exception {
                return Observable.fromIterable(movieDbApi.results);
            }
        }).doOnNext(new Consumer<Result>() {
            @Override
            public void accept(Result result) throws Exception {
                //adiciono na lista
                results.add(result);
            }
        });
    }

    @Override
    public Observable<String> getCountriesFromNetwork() {

        return getTopMoviesFromNetwork().concatMap(new Function<Result, Observable<ImdbApi>>() {
            @Override
            public Observable<ImdbApi> apply(Result result) {
                return imdbApiService.getCountry(result.title);
            }
        }).concatMap(new Function<ImdbApi, Observable<String>>() {
            @Override
            public Observable<String> apply(ImdbApi omdbApi) {
                return Observable.just(omdbApi.getCountry());
            }
        }).doOnNext(new Consumer<String>() {
            @Override
            public void accept(String s) {
                countries.add(s);
            }
        });
    }
}
