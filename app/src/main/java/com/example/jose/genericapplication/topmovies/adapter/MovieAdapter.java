package com.example.jose.genericapplication.topmovies.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jose.genericapplication.R;
import com.example.jose.genericapplication.topmovies.adapter.model.Movie;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ItemViewHolder> {

    private List<Movie> list;

    public MovieAdapter(List<Movie> moviesList){
        this.list = moviesList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_list_row,parent,false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        holder.itemName.setText(list.get(position).getName());
        holder.countryName.setText(list.get(position).getCountry());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView itemName;
        private TextView countryName;


        public ItemViewHolder(View itemView) {
            super(itemView);
            itemName = (TextView)itemView.findViewById(R.id.textView_fragmentlist_task_name);
            countryName = (TextView)itemView.findViewById(R.id.textView_fragmentlist_country_name);
        }
    }
}
