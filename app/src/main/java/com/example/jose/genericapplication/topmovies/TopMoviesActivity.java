package com.example.jose.genericapplication.topmovies;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.jose.genericapplication.R;
import com.example.jose.genericapplication.di.App;
import com.example.jose.genericapplication.topmovies.adapter.MovieAdapter;
import com.example.jose.genericapplication.topmovies.adapter.model.Movie;
import com.example.jose.genericapplication.topmovies.mvp.TopMoviesMVP;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TopMoviesActivity  extends AppCompatActivity implements TopMoviesMVP.View{

    private final String TAG = TopMoviesActivity.class.getName();

    // Initializations
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.listActivity_rootView)
    ViewGroup viewGroup;

    private MovieAdapter movieAdapter;
    private List<Movie> movieList = new ArrayList<>();

    @Inject
    TopMoviesMVP.Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_movies);
        ButterKnife.bind(this);

        //TODO IMPORTANTE SABER: -> só farei injecçãoo de dependencias aqui caso chame dependencias do gradlle como retrofit, picasso entre outros..., de resto o desenvolvimento é o normal
        // só poderei fazer uma injeção de cada vez numa activity dessa forma terei que arranjar uma forma de os módulos estarem separados, neste caso tenho o application module que contem
        // contexts etc com coisas de internet (internet modules)
        //TODO possivel: -> resolução: usar subcomponents??
        ((App) getApplication()).getInternetComponent().inject(this); //vai injectar também a application component(virtual)

    /*

        O dagger ajuda a que não seja necessário estar a inicializar todas as views no onCreate, o que a nível de testes e legibilidade seria péssimo

    */


        movieAdapter = new MovieAdapter(movieList);
        recyclerView.setAdapter(movieAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.setView(this);
        presenter.loadData();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.rxUnsubscribe();
        movieList.clear();
        movieAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateData(Movie movie) {
        movieList.add(movie);
        movieAdapter.notifyItemInserted(movieList.size()-1);
    }

    @Override
    public void showSnackBar(String s) {
        Snackbar.make(viewGroup,s,Snackbar.LENGTH_SHORT).show();
    }
}
