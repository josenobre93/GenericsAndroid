package com.example.jose.genericapplication.http.modules;

import com.example.jose.genericapplication.http.services.MovieDbApiService;
import java.io.IOException;
import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/*

    Para obter mais informação dos filmes mais populares(informação que vem de outra API)

 */
@Module
public class ImdbApiModule {


    public final String BASE_URL = "http://www.omdbapi.com";


    @Provides
    public OkHttpClient provideClient(){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return new OkHttpClient.Builder().addInterceptor(interceptor).build();

        /* Não precisamos de colocar um interceptor pois não precisamos de passar uma API key
           cada vez que quero fazer um request
        */
    }


    public MovieDbApiService provideMovieService(){
        return provideRetrofit(BASE_URL,provideClient()).create(MovieDbApiService.class);
    }


    /*

        AUX method

     */

    @Provides
    public Retrofit provideRetrofit(String baseURL, OkHttpClient client){
        return new Retrofit.Builder()
                .baseUrl(baseURL)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}
