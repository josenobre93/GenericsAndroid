package com.example.jose.genericapplication.http.services;

import com.example.jose.genericapplication.http.model.MovieDbApi;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/*

    Para obter o nome dos filmes mais populares
 */

public interface MovieDbApiService {

    @GET("top_rated")
    Observable<MovieDbApi> getTopRatedMovies(@Query("page") Integer page); // passo a página pois a paginação é feita no lado do servidor
}
