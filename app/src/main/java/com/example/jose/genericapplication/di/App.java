package com.example.jose.genericapplication.di;

import android.app.Application;

import com.example.jose.genericapplication.http.modules.ImdbApiModule;
import com.example.jose.genericapplication.http.modules.MovieDBApiModule;
import com.example.jose.genericapplication.topmovies.di.TopMoviesModule;

public class App extends Application {

    private InternetComponent internetComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        internetComponent = DaggerInternetComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .imdbApiModule(new ImdbApiModule())
                .movieDbApiModule(new MovieDBApiModule())
                .topMoviesModule(new TopMoviesModule())
                .build();

        //TODO acrescentar aqui outros comeponentes que envolvao módulos de outra dependencia que não relacionada com a internet como o Picasso etc...

    }

    public InternetComponent getInternetComponent() {
        return internetComponent;
    }
}
